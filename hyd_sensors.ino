/*

Este código es para el hydra de Eurocircuits, que tiene
Un RN4020 para BLE, glucometro cableado, presion sanguinea 
y circuiteria para leer ECG, OJO no para leer el ECG Bluetooth

*/



#include <SoftwareSerial.h>
#define MAX_MEASURES 16
#define GLUC_DATA_LENGTH 7
#define PRESSURE_DATA_LENGTH 8  
#define GLUC_ENABLE 6

#define ECG_PIN A0
#define PEAK_THRESHOLD 800
#define STREAM_SAMPLE_TIME 30

/*

tramas como *tipo_de_sensor,dato,dato,...dato!

tipo_de_sensor 

 0 ----- Glucometro
 1 ----- Tensiómetro
 2 ----- ECG
 


*/

#define GLUC_HEAD  0
#define PRESSURE_HEAD 1
#define ECG_HEAD   2



SoftwareSerial GLUC(7,8); //RX TX
SoftwareSerial PRESSURE(2,3);
//SoftwareSerial BLE(9,10);
int glucoseLength;
// flags de lectura y de envio
//////         VARIABLES PARA GLUCOMETRO     ///////

bool read_gluc = true, gluc_send = false;
// Anyo,mes,dia,hora,minuto,glucosa,meridian
int glucData[MAX_MEASURES][GLUC_DATA_LENGTH];

//////      VARIABLES PARA TENSIOMETRO     ////////
bool pressure_send = false, read_pressure = true;
int pressure_length=0;
uint8_t pressureData[MAX_MEASURES][PRESSURE_DATA_LENGTH];
//////      VARIABLES PARA RN4020     ////////

bool temp_send = false, oxi_send = false;
//////      VARIABLES PARA ECG     ////////

bool ecg_send = false;

int ecg_value = 0,ecg_value_prev = 0;
bool ecg_peak = 0;
uint32_t pulse = 0,pulse_acum = 0;
int pulse_time = 0, pulse_time_prev = 0,pulse_time_diff;
int peak_count = 0;

float stream_time,stream_time_prev,stream_time_diff;

void setup(){

  Serial.begin(19200);
  PRESSURE.begin(19200);
  GLUC.begin(1200);

  pinMode(GLUC_ENABLE, OUTPUT);
  pinMode(ECG_PIN,INPUT);


  digitalWrite(GLUC_ENABLE, HIGH);

}

void loop(){

  //delayMicroseconds(300);
  ecgMeasure();

  if(Serial.available()>0){
    char cmd= Serial.read();
    /*
    Serial.println();
    Serial.print("Cmd: ");
    Serial.println(cmd);
    */
    switch (cmd){
      case 'R':
      case 'r':
        GLUC.listen();
        gluc_send = true;
        break;
      case 'P':
      case 'p':
        PRESSURE.listen();
        pressure_send = true;
        break;
      case 'E':
      case 'e':
        ecg_send = !ecg_send;
        break;
        /*
      case 'O':
      case 'o':
        oxi_send = true;
        break;
      case 'T':
      case 't':
        temp_send = true;
        break;
        */
      default:
        // do something
        break;
    }
  }

  
  
  if(read_gluc){
    getGlucose();
    delay(500); 
  }
  if(read_pressure){
    getPressure();
    delay(500);    
  }

  if(gluc_send && !read_gluc ){
      
    sendGlucose();

  }
  
  if(pressure_send){

    sendPresure();

  }
  stream_time = millis();
  stream_time_diff = stream_time - stream_time_prev;
  
  if(stream_time_diff < STREAM_SAMPLE_TIME){      
    if(ecg_send){
      
      sendECG();
    }
  }
  if(oxi_send){

    sendOxi();
  }
  if(temp_send){

    sendTemp();
  }
  stream_time_prev = stream_time;
} // fin loop


void sendOxi(){


}
void sendTemp(){


}
void ecgMeasure(){

  ecg_value = analogRead(ECG_PIN);

  if(ecg_value > PEAK_THRESHOLD && ecg_value_prev <= PEAK_THRESHOLD){
      
    peak_count++;
    pulse_time = millis();
    pulse_time_diff = pulse_time - pulse_time_prev;
    pulse_acum += 60000 / pulse_time_diff;
    ecg_peak = 1;
    pulse_time_prev = pulse_time;
    if(peak_count > 2){

      pulse = pulse_acum / peak_count;
      pulse_acum = 0;
      peak_count = 0;
        
    }

  }
  else if(ecg_value_prev > PEAK_THRESHOLD && ecg_value <= PEAK_THRESHOLD){
      
      ecg_peak = 0;
  }

  ecg_value_prev = ecg_value;
}

void sendECG(){

  ecg_value = analogRead(ECG_PIN);

  Serial.print('*');
  Serial.print(ECG_HEAD);
  Serial.print(",");
  Serial.print(ecg_value);
  Serial.print(",");
  Serial.print(pulse);
  /*
  for(int i=0; i<4; i++){   
    Serial.print(',');
    Serial.print(0);   
  }*/
  Serial.println('!');
}

void sendPresure(){
  Serial.println();
  for(int i;i<pressure_length;i++){
    Serial.print("*");
    Serial.print(PRESSURE_HEAD);
    for(int j=0; j<8; j++){
      Serial.print(",");
      Serial.print(pressureData[i][j]);
    }
    Serial.println("!");
  }
  Serial.print("#");
  pressure_send = false;
  
}

void sendGlucose(){
  Serial.println();
  for(int j=0; j<glucoseLength; j++){
    Serial.print("*");
    Serial.print(GLUC_HEAD);
    for(int i=0; i<GLUC_DATA_LENGTH; i++){
      Serial.print(",");
      Serial.print(glucData[j][i]);
    }        
    Serial.println("!");  
  }
  Serial.print("#");
  gluc_send = false; // reseteo el flag de lectura 
  delay(1);
}

void getGlucose(){
  // Lectura de info del glucometro 
  delay(10);
  GLUC.write("U"); // Start communication command.
  delay(800); // Wait while receiving data.
  GLUC.print(F("\n"));

  if (GLUC.available() > 0){
    glucoseLength = GLUC.read();// The protocol sends the number of measures
    
    if (glucoseLength > 16){
      glucoseLength = 16;
    }

    GLUC.read(); // Read one dummy data
    for (int i = 0; i < glucoseLength; i++){
      for(int j=0; j<GLUC_DATA_LENGTH; j++){
        glucData[i][j] = GLUC.read();
        if(j == 4){
          GLUC.read();             
        } 
      }
      if(glucData[i][6] != 170 && glucData[i][6] != 187){        
        return;
      }
      GLUC.read();
      GLUC.read();
    }
    read_gluc = false; // se lee la primera vez que se pueda y hasta que no haya reset no se vuelve a leer pues no se vuelve a validar el flag
  }
}


void getPressure(void){ 

  unsigned char _data;
  int ia=0;
  pressure_length=0;
  PRESSURE.begin(19200);
  PRESSURE.write(0xAA);
  delayMicroseconds(1);
  PRESSURE.write(0x55);
  delayMicroseconds(1);
  PRESSURE.write(0x88);
  delay(800);
  PRESSURE.print("\n");
  if (PRESSURE.available() > 0){ // The protocol sends the measures 
    for (int i = 0; i<4; i++){ // Read four dummy data  
      PRESSURE.read();
    }
    
    while(_data != 0xD1){
      
      if (ia==0){      
        _data = PRESSURE.read();      
      }
   
      pressureData[pressure_length][3] = swap(_data);          //year      
      pressureData[pressure_length][4] = swap(PRESSURE.read());//month     
      pressureData[pressure_length][5] = swap(PRESSURE.read());//day       
      pressureData[pressure_length][6] = swap(PRESSURE.read());//hour      
      pressureData[pressure_length][7] = swap(PRESSURE.read());//minutes   
      pressureData[pressure_length][0] = swap(PRESSURE.read()) + 30;//systolic  
      pressureData[pressure_length][1] = swap(PRESSURE.read());//diastolic 
      pressureData[pressure_length][2] = swap(PRESSURE.read());//pulse     
      pressure_length++;
      ia=1;
      for (int i = 0; i<4; i++){ // CheckSum 1
        PRESSURE.read();
      }
      
      _data = PRESSURE.read();
    }
    for (int i = 0; i<3; i++){ // CheckSum 2
      PRESSURE.read();
    } 
    
  }
  read_pressure = false;
}// fin get pressure
  //! Swap data for blood pressure mesure

char swap(char _data){
  char highBits = (_data & 0xF0) / 16; 
  char lowBits =  (_data & 0x0F) * 16; 
  return ~(highBits + lowBits);
}